//
// Created by "Yaokai Liu" on 12/10/22.
//

#include "xstring.h"

xSize strcpy(xuByte * _dest, const xuByte * _src, xuByte _end_char) {
    if (!_dest || !_src) {
        return 0;
    }
    xSize _len = 0;
    while (_src[_len] && _src[_len] != _end_char) {
        _dest[_len] = _src[0];
        _len++;
    }
    return _len;
}
xSize strlen(const xuByte * _string, xuByte _end_char) {
    if (!_string) {
        return 0;
    }
    xSize _len = 0;
    while (_string[_len] && _string[_len] != _end_char) {
        _len++;
    }
    return _len;
}
xLong strcmp(const xuByte * _str1, const xuByte * _str2, xuByte _end_char) {
    if (_str1 == _str2) {
        return 0;
    }
    if (!_str1) {
        return -(xLong) strlen(_str2, _end_char);
    }
    if (!_str2) {
        return (xLong) strlen(_str1, _end_char);
    }
    xSize same_len = 0;
    while (_str1[same_len] && _str1[same_len] != _end_char) {
        if (_str2[same_len] == _str1[same_len])
            same_len ++;
    }
    if ((!_str1[same_len]) || (_str1[same_len] == _end_char)) {
        return - (xLong) strlen(_str2 + same_len, _end_char);
    }
    if ((!_str2[same_len]) || (_str2[same_len] == _end_char)) {
        return (xLong) strlen(_str1 + same_len, _end_char);
    }
    return 0;
}
