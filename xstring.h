//
// Created by "Yaokai Liu" on 12/10/22.
//

#ifndef XC_XSTRING_H
#define XC_XSTRING_H

#include "xtypes.h"

xSize strcpy(xuByte * _dest, const xuByte * _src, xuByte _end_char);
xSize strlen(const xuByte * _string, xuByte _end_char);
xLong strcmp(const xuByte * _str1, const xuByte * _str2, xuByte _end_char);

#endif //XC_XSTRING_H
