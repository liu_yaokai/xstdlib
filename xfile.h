//
// Created by Yaokai Liu on 12/17/22.
//

#ifndef X_XFILE_H
#define X_XFILE_H

#include "xtypes.h"

typedef struct {
    xuByte      open_flags;
    xPtrDiff    position;
    void *      fsi; // file system interface
    void *      fdi; // file driver interface
} xFile;

#endif //X_XFILE_H
